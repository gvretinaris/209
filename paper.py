#!/usr/bin/env python
import numpy as np
from scipy.integrate import odeint
from scipy.integrate import simpson, quad
from scipy import constants as cs
import matplotlib.pyplot as plt
import sys
from scipy.interpolate import pchip_interpolate,interp1d
import warnings
import os
import pymp
# from numbalsoda import lsoda_sig, lsoda
# from numba import njit, cfunc, types, carray
import time

if not sys.warnoptions:
    warnings.simplefilter("ignore")
    os.environ["PYTHONWARNINGS"] = "ignore" # Also affect subprocesses
start_time = time.time()

pymp.config.nested = True
from scipy.signal import find_peaks

# def rhs(r,y,dy,n):
#     dy[0] = -pow(y[1],n) * r * r
#     dy[1] = y[0] / (r * r)

# args_dtype = types.Record.make_c_struct([
#                     ('p', types.float64)])
    
# def create_jit_rhs(rhs, args_dtype):
#     jitted_rhs = njit(rhs)
#     @cfunc(types.void(types.double,
#              types.CPointer(types.double),
#              types.CPointer(types.double),
#              types.CPointer(args_dtype)))
#     def wrapped(t, u, du, user_data_p):
#         # unpack p and arr from user_data_p
#         user_data = carray(user_data_p, 1)
#         p = user_data[0].p  
#         # then we call the jitted rhs function, passing in data
#         jitted_rhs(t, u, du, p) 
#     return wrapped

# rhs_cfunc = create_jit_rhs(rhs,args_dtype)
# funcptr = rhs_cfunc.address

def LaneEmden(y, r, n):
    return np.array([-y[1] ** n * r * r, y[0] / (r * r)])


dx = 1e-3
initial_state = [
    -(1.0 / 3.0) * dx**3.0,
    1.0 - (1.0 / 6.0) * dx**2.0,
]
x1 = np.arange(dx, 15, dx)  # This is an np.array object

g = np.round(np.arange(1.25,1.805,0.0125),5)
x = []
for i in range(g.size):
    x.append(x1)

rho_c = np.arange(1,3,0.01)

# S = np.zeros(g.size)
# S1 = np.zeros(g.size)
# S2 = np.zeros(g.size)
# aV = np.zeros(g.size)
# Stilde = np.zeros((rho_c.size,g.size))
# M = np.zeros(g.size)

G = 6.7e-57
K = 1/(15*0.511*1e6*np.pi**2)*pow(3*np.pi**2/(938.26e6*2),5./3.)
# rho_0 = 1
rho_0 =1.5e9*(1.9732705e-7)**3/(1.7826627e-36)
# rho_c = rho_0/rho_c
denominator = np.sqrt(4*np.pi*G/K)

# fig1 = plt.figure(1,figsize=(8, 6), dpi=100)
# ax1 = fig1.add_subplot()
# fig1.suptitle('Lane-Emden', fontsize=18)
# ax1.set_xlabel(r'$\xi$')
# ax1.set_ylabel(r'$\theta(\xi)$')
# ax1.grid()
# fig2 = plt.figure(2,figsize=(8, 6), dpi=100)
# ax2 = fig2.add_subplot()
# fig2.suptitle('Modal fraction', fontsize=18)
# ax2.tick_params(top=True,bottom=True,left=True,right=True,direction='in')
# ax2.set_yticks(np.arange(0, 1.11, .1))
# ax2.set_xticks(np.hstack([np.arange(0,1.5,0.2),[1.5]]))
# ax2.set_ylim([0,1.1])
# ax2.set_xlim([0,1.5])
# ax2.set_xlabel("$\kappa$")
# ax2.set_ylabel(r"$\tilde{f}(\kappa)$")

S = pymp.shared.array(g.shape)
S1 = pymp.shared.array(g.shape)
S2 = pymp.shared.array(g.shape)
M = pymp.shared.array(g.shape)
aV = pymp.shared.array(g.shape)
Stilde = pymp.shared.array((rho_c.size,g.size))
Mtilde = pymp.shared.array((rho_c.size,g.size))

with pymp.Parallel(6) as p1:
    with pymp.Parallel(10) as p2:
        for index in p1.range(g.size):
            for indx in p2.range(rho_c.size):
                gamma = g[index]
                n = 1 / (gamma - 1)

                a = np.sqrt(
                    K * gamma / (4 * np.pi * G * (gamma - 1))* rho_0 ** (gamma - 2))
                aV[index] = a
                # a2 = 1/np.sqrt(
                    # gamma* rho_0 ** (gamma - 2) / ((gamma - 1)))

                # plt.figure(1,figsize=(8, 6), dpi=100)
                # plt.suptitle('Lane-Emden', fontsize=18)
                initial_state = np.array([
                    -(1.0 / 3.0) * dx**3.0,
                    rho_c[indx]**(1/n)*(1.0 - (1.0 / 6.0) * dx**2.0),
                ])
                
                # args = np.array(n,dtype=args_dtype)

                # sol,_ = lsoda(
                #     funcptr,
                #     initial_state,
                #     x[index],
                #     data = args
                # )
                
                sol = odeint(
                    LaneEmden,
                    initial_state,
                    x[index],
                    args=(n,),
                    rtol=1e-12,
                    atol=1e-15,
                )
                idx_nan = np.where(np.isnan(sol))[0]
                idx = np.array([])
                # Cutting numerical artefacts
                if idx_nan.size == 0:
                    idx = np.where(sol[:, 1] < 0)[0]
                    if idx.size != 0:
                        idx = idx[0]
                        sol = sol[:idx, :]
                        x[index] = x[index][:idx]
                    else:
                        pass
                elif idx_nan.size != 0:
                    idx = idx_nan[0]
                    sol = sol[:idx, :]
                    x[index] = x[index][:idx]

                # if gamma in np.array([1.25,1.4,1.7],dtype=np.float64):
                #     ax1.plot(x[index], sol[:, 1], label=fr'$\gamma={gamma:.3f}$')
                    # ax1.legend(loc='best')
                    # ax1.set_xlabel(r'$\xi$')
                    # ax1.set_ylabel(r'$\theta(\xi)$')
                    # ax1.grid()

                R = a * x[index][-1]
                k_min = np.pi / x[index][-1]
                scaler = np.sqrt((gamma-1)/gamma)

                xnew = np.linspace(0,x[index][-1],5000)
                ynew = pchip_interpolate(x[index],sol[:,1],xnew)
                N = 1001
                h = np.zeros(N)
                h1 = np.zeros(N)
                h2 = np.zeros(N)
                kappa = np.linspace(k_min, 50*k_min, N)
                for i, k in enumerate(kappa):
                    h[i] = ((4*np.pi*rho_0*a**3/k)\
                             *simpson(pow(ynew,n)*np.sin(k*xnew)*xnew,x=xnew))**2

                    k1 = k/0.95
                    h1[i] = ((4*np.pi*rho_0*a**3/k1)\
                             *simpson(pow(ynew,n)*np.sin(k1*xnew)*xnew,x=xnew))**2

                    k2 = k/1.05
                    h2[i] = ((4*np.pi*rho_0*a**3/k2)\
                             *simpson(pow(ynew,n)*np.sin(k2*xnew)*xnew,x=xnew))**2



                f_tilde =  h/ h[0]
                f_tilde1 = h1/ h1[0]
                f_tilde2 = h2/ h2[0]
                # plt.figure(2,figsize=(8, 6), dpi=100)
                # plt.suptitle('Modal fraction', fontsize=18)
                # if gamma in np.array([1.25,1.4,1.7],dtype=np.float64):
                #     ax2.plot(
                #         kappa*scaler,
                #         f_tilde,
                #         label=f"$\gamma$={gamma:.2f}")


                    # plt.tick_params(top=True,bottom=True,left=True,right=True,direction='in')
                    # plt.yticks(np.arange(0, 1.11, .1))
                    # plt.xticks(np.hstack([np.arange(0,1.5,0.2),[1.5]]))
                    # plt.ylim([0,1.1])
                    # plt.xlim([0,1.5])
                    # plt.xlabel("$\kappa$")
                    # plt.ylabel(r"$\tilde{f}(\kappa)$")

                if indx == 0:
                    S[index] = -4*np.pi*a**(-3)*simpson(f_tilde*np.log(f_tilde)*kappa**2,x=kappa)

                    S1[index] = -4*np.pi*a**(-3)*simpson(f_tilde1*np.log(f_tilde1)*(kappa/0.95)**2,
                                                         x=kappa/0.95)
                    S2[index] =  -4*np.pi*a**(-3)*simpson(f_tilde2*np.log(f_tilde2)*(kappa/1.05)**2,
                                                          x=kappa/1.05)
                    M[index] = 4*np.pi*rho_0*a**3*simpson(pow(ynew,n)*pow(xnew,2),x=xnew)

                    
                
                Mtilde[indx,index] = 4*np.pi*rho_0*a**3*rho_c[indx]**(3*(gamma-2)/2+1)\
                    *simpson(pow(ynew,n)*pow(xnew,2),x=xnew)\
                    *denominator**3*rho_0**(2-3/2*gamma)*rho_c[indx]**(2-3/2*gamma)
                  
                Stilde[indx,index] = -4*np.pi*a**(-3)*rho_c[indx]**(-3*(gamma-2)/2-1)\
                    *simpson(f_tilde*np.log(f_tilde)*kappa**2,x=kappa)\
                    /((rho_0*rho_c[indx])**(3-3/2*gamma)*denominator**3)
                
                # print(Stilde[indx,index])

print(f"Time = {time.time()-start_time}")
# ax1.legend(loc='best')
# ax2.legend(loc='best')
g2  = np.linspace(g[0],g[-1],g.size*10)
Sp1 = interp1d(g,S/(rho_0**(3-3/2*g)*denominator**3),'cubic'); Sp1 = Sp1(g2)
Sp2 = interp1d(g,S*aV**3,'cubic'); Sp2 = Sp2(g2)
# Sp3 = np.zeros((g2.size,rho_c.size))
# for i in range(rho_c.size):
#     Sp3f = interp1d(g,Stilde[:,i]/(rho_c[i]**(2-3/2*g)*denominator**3),'quadratic')
#     Sp3[:,i] = Sp3f(g2)
S1p1 = interp1d(g,S1*aV**3,'cubic'); S1p1 = S1p1(g2)
S2p1 = interp1d(g,S2*aV**3,'cubic'); S2p1 = S2p1(g2)              
Mp1 = interp1d(g,M*denominator**3*rho_0**(2-3/2*g)/(200),'cubic'); Mp1 = Mp1(g2)
# x,y = np.meshgrid(rho_c,g)

plt.figure(5,figsize=(8,6),dpi=100)
plt.suptitle('Mass Contour', fontsize=18)
#levels = np.arange(130,250,20)
plt.contour(Mtilde,
           extent=[min(g),max(g),min(rho_c),max(rho_c)],
           levels= np.linspace(120,270,20),
           colors='black',
           linewidths=0.75)#,levels=levels)
plt.contourf(Mtilde,
             extent=[min(g),max(g),min(rho_c),max(rho_c)],
             levels=np.linspace(120,270,20),
             cmap=plt.cm.gray)
plt.ylabel(r"$\dfrac{\rho_0}{\rho_c}$",rotation=0,labelpad=10,
           fontsize=12)
plt.xlabel("$\gamma$",fontsize=12)
plt.axvline(4/3,color='k',linewidth=2.5)
plt.colorbar(ticks=np.arange(120,261,20))
plt.tick_params(top=True,bottom=True,left=True,right=True,direction='in')
plt.yticks(np.arange(1, 2.81, .2))
plt.xticks(np.arange(1.3,1.8, .1))
# g = g2

plt.savefig('MassContour.pdf',dpi=100)

plt.figure(6,figsize=(8,6),dpi=100)
plt.suptitle('Configurational Entropy Contour', fontsize=18)
#levels = np.arange(130,250,20)
plt.contour(Stilde,
            extent=[min(g),max(g),min(rho_c),max(rho_c)],
            levels= np.linspace(0.45,1.4,20),
            colors='black',
            linewidths=0.75)#,levels=levels)
plt.contourf(Stilde,
             extent=[min(g),max(g),min(rho_c),max(rho_c)],
             levels=np.linspace(0.45,1.4,20),
             cmap=plt.cm.gray)

plt.ylabel(r"$\dfrac{\rho_0}{\rho_c}$",rotation=0,labelpad=10)
plt.xlabel("$\gamma$")
plt.axvline(4/3,color='k',linewidth=2.5)
plt.colorbar()
plt.tick_params(top=True,bottom=True,left=True,right=True,direction='in')
plt.yticks(np.arange(1, 2.81, .2))
plt.xticks(np.arange(1.3,1.8, .1))

plt.savefig('CEContour.pdf',dpi=100)
g = g2                      
#exit()

plt.figure(3,figsize=(8, 6), dpi=100)
plt.suptitle('Configurational Entropy and Mass', fontsize=18)
plt.plot(g,Sp1,'r',label=r"$S\rho_0^{-1}/\left(\left(\frac{K}{4 \pi G} \right)^{-\frac{3}{2}}\rho_c^{2-\frac{3}{2}\gamma} \right)$")
plt.plot(g,Mp1,'g:',label=r"$M/\left(200 \left(\frac{K}{4 \pi G} \right)^{\frac{3}{2}} \rho_c^{\frac{3}{2}\gamma-2} \right)$")
plt.legend()
plt.tick_params(top=True,bottom=True,left=True,right=True,direction='in')
plt.yticks(np.arange(0.4, 1.31, .1))
plt.xticks(np.arange(1.25,1.71,0.05))
plt.ylim([0.4,1.3])
plt.xlim([1.25,1.7])
plt.xlabel("$\gamma$")
plt.savefig("ConfEntropyAndMass.pdf",dpi=100)

plt.figure(4,figsize=(8, 6), dpi=100)
plt.suptitle('Configurational Entropy', fontsize=18)

plt.plot(g,S1p1,':',label="$\pi$/(0.95)R")
idx = np.argmax(S1p1)
plt.plot(g[idx],S1p1[idx],'rv')
idx = np.argmin(S1p1)
plt.plot(g[idx],S1p1[idx],'bo',markerfacecolor='white',
         markeredgecolor='blue')

plt.plot(g,Sp2,'k',label="$\pi$/(1.00)R")
idx = np.argmax(Sp2)
plt.plot(g[idx],Sp2[idx],'rv')
idx = np.argmin(Sp2)
plt.plot(g[idx],Sp2[idx],'bo',markerfacecolor='white',
         markeredgecolor='blue')

plt.plot(g,S2p1,'r-.',label="$\pi$/(1.05)R")
idx = np.argmax(S2p1)
plt.plot(g[idx],S2p1[idx],'rv')
idx = np.argmin(S2p1)
plt.plot(g[idx],S2p1[idx],'bo',markerfacecolor='white',
         markeredgecolor='blue')


plt.axvline(4/3,color='gray',linestyle='--')
plt.text(4/3+0.005,4.625,"4/3")
plt.axvline(5/3,color='gray',linestyle='--')
plt.text(5/3+0.005,4.625,"5/3")

plt.legend()
plt.tick_params(top=True,bottom=True,left=True,right=True,direction='in')
plt.yticks(np.arange(4.6, 5.5, .1))
plt.xticks(np.arange(1.3,1.8,.1))
plt.ylim([4.6,5.6])
plt.xlim([1.25,1.75])
plt.xlabel("$\gamma$")
plt.ylabel(r"$S\alpha^3$")

plt.savefig("ConfEntropy.pdf",dpi=100)

# plt.figure(5,figsize(8,6),dpi=100)
# plt.suptitle('Configurational Entropy Contour', fontsize=18)
# x,y = np.meshgrid(g,np.arange(0.333,1,0.001))
# plt.contour(x,y,S)
#plt.show()
