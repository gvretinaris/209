\documentclass[10pt,xcolor=table]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usetikzlibrary{shapes.arrows}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{listings}
\lstset{language=python,basicstyle=\ttfamily}
\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}
\renewcommand{\Re}{\operatorname{Re}}
\renewcommand{\Im}{\operatorname{Im}}
\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\definecolor{mSybilaRed}{HTML}{990000}

\tikzset{
	myarrow/.style={
		draw,
		single arrow,
		minimum height=5ex,
		line width=3pt,
		single arrow head extend=0.1ex
	}
}
\newcommand{\arrowup}{%
	\tikz [baseline=-0.5ex]{\node [myarrow,rotate=90, single arrow head extend=2mm,inner sep=.1mm] {};}% or: single arrow head indent=⟨length⟩
}
\newcommand{\arrowdown}{%
	\tikz [baseline=-1ex]{\node [myarrow,rotate=-90,  yscale=.5, single arrow head extend=1mm,inner sep=.1mm] {};}
}





\setbeamercolor{title separator}{
	fg=mSybilaRed
}
\graphicspath{{/home/gvretinaris/Desktop/work/BScThesis/thesis/imgs/}}
\setbeamercolor{progress bar}{%
	fg=mSybilaRed,
	bg=mSybilaRed!90!black!30
}

\setbeamercolor{progress bar in section page}{
	use=progress bar,
	parent=progress bar
}

\setbeamercolor{alerted text}{%
	fg=mSybilaRed
}

\title[Code Review]{Retracing the steps \\ of Gleiser et al. on:}

\titlegraphic{\hfill\includegraphics[height=2cm]{LogoAUTH300ppi.png}}

\date{}
\author{Vretinaris Giorgos}
\institute{Physics Department @ A.U.Th.}

\subtitle{Stability bounds on compact astrophysical\\
	objects from information-entropic measure}
\date{\today}

\setbeamertemplate{footline}
{
	\leavevmode
	\hbox{
		\begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}
			\usebeamerfont{author in head/foot}\insertshortauthor
		\end{beamercolorbox}
		
		\begin{beamercolorbox}[wd=.7\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}
			\usebeamerfont{author in head/foot}\insertshorttitle
		\end{beamercolorbox}
		
		\begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}
			\insertframenumber{} / \inserttotalframenumber
		\end{beamercolorbox}
	}
}

\begin{document}
	
	\maketitle
	
	\section{Cold White Dwarfs and the Chandrasekhar limit}
	
	\begin{frame}[fragile]{Lane-Emden Equation}

	
	
\begin{center}
\centering{
\begin{columns}[c] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment
\hspace{-0.5cm}\column{.3\textwidth}{ % Right column and width

\onslide<1->\begin{equation*}
	\frac{1}{\xi^2} \frac{d}{d\xi}\left(\xi^2 \frac{d\theta}{d\xi}\right)+\theta^{1/(\gamma-1)}=0
\end{equation*}
\only<2>{\begin{align*}
	&\theta(0) = 1 \qquad \theta'(0) = 0 \\
	&\frac{d \theta}{d \xi} = \frac{w}{\xi^2} \\
	&\frac{d w}{d\xi} = -\theta^{1/(\gamma-1)}\xi^2
\end{align*}}
\onslide<3->\begin{lstlisting}[basicstyle=\scriptsize]
def LaneEmden(y, r, n):
    return np.array(
    [-y[1]**n*r*r,
    y[0]/(r*r)])
dx=1e-3
initial_state=[
-(1.0/3.0)*dx**3.0,
1.0-(1.0/6.0)*dx**2.0]
x = np.arange(dx,15,dx)
sol = odeint(
LaneEmden,
initial_state,
x,
args=(n,),
rtol=1e-12,
atol=1e-15,
)\end{lstlisting}
}
\hfill\column{.7\textwidth}{ % Left column and width
\onslide<4->\hspace*{1cm}\includegraphics[width=\textwidth]{LE.pdf}}
\end{columns}}
\end{center}

\end{frame}

	\begin{frame}[fragile]{Modal fraction}
	
	
\begin{center}
\centering{
\onslide<1->\begin{align*}
	\tilde{f(}\kappa) = \frac{h(\kappa)}{h(\kappa_\mathrm{min})} \quad h(\kappa) = \left(\frac{4 \pi \rho_0 \alpha^3}{\kappa} \int_{0}^{\xi_R} \theta^{1/(\gamma-1)}(\xi) \sin(\kappa \xi) \xi d\xi \right)^2
\end{align*}
\begin{columns}[c] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment
\hspace{-0.5cm}\column{.3\textwidth}{ % Right column and width

\only<2>{\begin{align*}
	&\alpha = \sqrt{\frac{K \gamma \rho_0^{\gamma-2}}{4\pi G (\gamma - 1)}} \\
	& \xi_R = \frac{R}{\alpha} = \arg(\theta(\xi)=0) \\
	& \kappa_\mathrm{\min} = \frac{\pi}{\xi_R}
\end{align*}}
\onslide<3->\begin{lstlisting}[basicstyle=\scriptsize]
h=np.zeros(N)
x=x[:idx]; y=sol[:idx,1]
kappa=np.linspace(k_min,
                  50*k_min,
                  N)
for i,k in enumerate(kappa):
    h[i] = (
    (4*np.pi*rho_0*a**3/k)
    *simpson(pow(y,n)
    *np.sin(k*x)*x,x=x)
    )**2
f_tilde =  h/ h[0]
\end{lstlisting}
}
\hfill\column{.7\textwidth}{ % Left column and width
\onslide<3->\hspace*{1cm}\includegraphics[width=\textwidth]{modal.pdf}}
\end{columns}}
\end{center}

\end{frame}


	\begin{frame}[fragile]{Configurational Entropy}
	
\begin{center}
\centering{
\begin{columns}[c] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment
\column{.4\textwidth}{


\onslide<1->\footnotesize\begin{align*}
	S = &-4 \pi \alpha^{-3} \\
	&\int_{\kappa_\mathrm{min}}^{\infty} \tilde{f(}\kappa) \log(\tilde{f(}\kappa_\mathrm{min})) \kappa^2 d\kappa \\
	M =\hspace{2pt} &4 \pi \rho_0 \alpha^3 \\ 
	&\int_{0}^{\xi_R} \theta^{1/(\gamma-1)}(\xi)\xi^2 d\xi
\end{align*}
\begin{onlyenv}<2-3>
{\begin{lstlisting}[basicstyle=\scriptsize]
S[index] = -4*np.pi*a**(-3)
           *simpson(f_tilde
           *np.log(f_tilde)
           *kappa**2,x=kappa)
M[index] = 4*np.pi*rho_0*a**3
           *simpson(pow(y,n)
           *pow(x,2),x=x)
\end{lstlisting}
}\end{onlyenv}
\begin{onlyenv}<4>
	{\begin{lstlisting}[basicstyle=\scriptsize]
Stilde[indx,index] = -4*np.pi
        *a**(-3)*rho_c[indx]\
        **(-3*(gamma-2)/2-1)
        *simpson(f_tilde
        *np.log(f_tilde)
        *kappa**2,x=kappa)
        /((rho_0*rho_c[indx])
        **(3-3/2*gamma)
        *denominator**3)
\end{lstlisting}
}\end{onlyenv}
\begin{onlyenv}<5>
	{\begin{lstlisting}[basicstyle=\scriptsize]
Mtilde[indx,index] = 4*np.pi
     *rho_0*a**3*rho_c[indx]
     **(3*(gamma-2)/2+1)\
     *simpson(pow(y,n)
     *pow(x,2),x=x)\
     *denominator**3*rho_0
     **(2-3/2*gamma)
     *rho_c[indx]**(2-3/2*gamma)
\end{lstlisting}
}\end{onlyenv}


}
\hfill\column{.6\textwidth}{
\hspace{2cm}\only<2>{\includegraphics[width=\textwidth]{ConfEntropyAndMass.pdf}}
\hspace{2cm}\only<3>{\includegraphics[width=\textwidth]{ConfEntropy.pdf}}
\hspace{2cm}\only<4>{\includegraphics[width=\textwidth]{CEContour.pdf}}
\hspace{2cm}\only<5>{\includegraphics[width=\textwidth]{MassContour.pdf}}

}
\end{columns}}
\end{center}

	
\end{frame}


{
\setbeamertemplate{footline}{} 
\begin{frame}[standout]
	Thank you for your attention.
\end{frame}
}

\end{document}
